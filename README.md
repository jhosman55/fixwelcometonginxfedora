# How to fix bug Welcome to nginx on Fedora! in cPanel with engitron
Bug of updates Engitron and EPEL Centos

```
yum remove nginx
```
Edit the file /etc/yum.repos.d/epel.repo and add this line in the end of section [epel]

```
exclude=nginx*
```
Now run
```
/engintron.sh remove
yum clean all
rm -f /etc/nginx/conf.d/default.conf 
cd /  
rm -f engintron.sh  
wget --no-check-certificate https://raw.githubusercontent.com/engintron/engintron/master/engintron.sh  
bash engintron.sh install
```

if it's necesary, edit Edit your custom_rules for Nginx in WHM with the IP address

```
set $PROXY_DOMAIN_OR_IP "XXX.XXX.XXX.XXX";
```
Restart services

```
/engintron.sh disable
/engintron.sh enable
```

Verify nginx service and httpd, please search ports 80 and 443

```
netstat -putona | grep nginx
netstat -putona | grep http
```
